# SWEN90007

# Knowledge Base
## Git
### Initializing the repository
```bash
# init local git repository, do this under the path/folder of where you want your local git source repository to be
git init .
# specify username and email
git config user.name "<your name>" # For example: git config user.name "DI MAO"
git config user.email "<your email>" # For example: git config user.email "dmao1@student.unimelb.edu.au"
#Turn off https verification
git config http.sslVerify false
# add the remote repository
git remote add origin https://"<your username>"@bitbucket.org/swen90007_project/swen90007.git # For example: https://"<your username>"@bitbucket.org/swen90007_project/swen90007.git
# pull from remote repository
git pull origin master
```

### Pushing changes
```bash
# add new files
git add .
# commit the changes
git commit -m "<message>" # For example: git commit -m "Bug fixes"
# push to remote repository
git push -u origin master
```