var mongoose = require('mongoose');

var ProductSchema = mongoose.Schema({
	pid: {
		type: String,
		index: true
	},
	name:{
		type: String
	},
	price:{
		type: Number
	},
	description: {
		type: String
	},
	quantity:{
		type: Number
	},
	picture:{
		type: String
	}
});

var Product = module.exports = mongoose.model('Product', ProductSchema);
