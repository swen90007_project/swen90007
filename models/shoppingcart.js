var mongoose = require('mongoose');

var ShoppingCartSchema = mongoose.Schema({
	id: {
		type: String,
		index: true
	},
	product: [{
		pid: String, 
		name: String,
		price: Number,
		picture: String,
		quantity: Number
	}]
});

var ShoppingCart = module.exports = mongoose.model('ShoppingCart', ShoppingCartSchema);

module.exports.createShoppingCart = function(newShoppingCart, callback){
	newShoppingCart.save(callback);
}
