var mongoose = require('mongoose');

var DiscountSchema = mongoose.Schema({
	id: {
		type: String,
		index: true
	},
	name:{
		type: String
	},
	description:{
		type: String
	},
	discounttype: {
		type: String
	},
	userid:[
		String
	],
	offrate:{
		type: Number
	},
	productid:[{
		pid: String
	}],
	startdate:{
		type: Date
	},
	enddate:{
		type: Date
	}
});

var Discount = module.exports = mongoose.model('Discount', DiscountSchema);
