var mongoose = require('mongoose');
var bcrypt = require("bcryptjs");

var UserSchema = mongoose.Schema({
	email: {
		type: String,
		index: true
	},
	password:{
		type: String
	},
	name: {
		type: String
	},
	phonenumber:{
		type: Number
	},
	address:{
		type: String
	}
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports.comparePassword = function(password, hash, callback) {
    // EXAMPLE CODE!
    bcrypt.compare(password, hash, function(err, res) {
    // res == true 
    callback(null, res);
});
};


module.exports.createUser = function(newUser, callback){
	bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(newUser.password, salt, function(err, hash) {
        newUser.password = hash;
		newUser.save(callback);
    });
});
}

module.exports.updatePassword = function(uid, password, callback){
	bcrypt.genSalt(10, function(err, salt) {
    	bcrypt.hash(password, salt, function(err, hash) {
	      var updateData ={
	        password: hash
	      }
	      User.update({ _id: uid }, updateData, function(err,affected) {	        
	        callback(err,affected)
	      });
    	});
  });
}

