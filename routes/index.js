var express = require('express');
var router = express.Router();

var Product = require('../models/product');


/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { user: req.session.user });
});

router.get('/adminlogin', function(req, res, next) {
      res.render('adminlogin');
});


router.get('/productlist', function(req, res, next) {
  Product.find({},{}, function(err, docs){
  	//console.log(docs);
  	 res.render('productlist', { 
  	 	user: req.session.user,
      product: docs
  	 });
  });	
});

router.post('/adminlogin', function(req, res, next) {
  var username = req.body.username;
  var password = req.body.password;

  if(username == "admin"){
  	if(password == "1"){
      res.redirect('/admin');
    }
  }
});



module.exports = router;
