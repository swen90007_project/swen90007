var express = require('express');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var bcrypt = require("bcryptjs");
var async = require("async");
var crypto = require("crypto");
var nodemailer = require('nodemailer');
var smtpTransport = require("nodemailer-smtp-transport")

var User = require('../models/user');
var Shoppingcart = require('../models/shoppingcart'); 
var Product = require('../models/product'); 
var Discount = require('../models/discount');
var router = express.Router();


var options = {
    service: 'gmail',
    auth: {
        user: 'maodi101@gmail.com',
        pass: '15150999623maodi'
    }
  };

passport.use(new LocalStrategy(
  function(username, password, done) {
    User.findOne({ email: username }, function (err, user) {
      if (err) { return done(err); }
      if (!user) {
        return done(null, false, { message: 'Email is not registed.' });
      }
      User.comparePassword(password, user.password, function(err, isMatch){
        if(isMatch){
            console.log("user success login");
            
            return done(null, user, {message: 'success'});
        }else {
            return done(null, false, { message: 'Incorrect password.' });
        }
      })
    });
  }
));

passport.serializeUser(function(user, done) {
  done(null, user.id);
});

passport.deserializeUser(function(id, done) {
  User.findById(id, function(err, user) {
    done(err, user);
  });
});



/* GET users listing. */
router.get('/', authentication);
router.get('/', function(req, res, next) {
  res.render('index');
});

/* GET login page. */
router.get('/login', notAuthentication);
router.get('/login', function(req, res, next) {
  res.render('login', { 
    user: null,
    error: req.session.error  
  });
});


/* Post login form */
router.post('/login', function(req, res, next) {
  passport.authenticate('local', function(err, user, info) {
    if (err) { return next(err); }
    if (!user) { 
      console.log(info);
      return res.render('login', { user: null, error: info.message}); }
      req.logIn(user, function(err) {
      if (err) { 

        return next(err); 
      }
      //console.log(user);

      //set session
      req.session.user = user;
      req.session.success = "login success!";
      req.session.error = null;
      if (req.body.remember_me == 'remember_me') {
            req.session.cookie.maxAge = 3600000;
      }

      return res.redirect('/users/profile');
    });

  })(req, res, next);
});

router.get('/logout', authentication);
router.get('/logout', function(req, res){
  req.session.user = null;
  req.session.error = null;
  res.redirect('/users/login');
});



router.get('/signup', function(req, res){
  res.render('signup', {
    user: null,
    errors: null
  });
});



/* Post registeration form*/
router.post('/signup', function(req, res){
  var name = req.body.name;
  var username = req.body.username;
  var password = req.body.password;
  var comfirmpassword = req.body.password2;
  var phonenumber = req.body.phonenumber;
  var address = req.body.address;

  //validation
  req.checkBody('username', 'Email is required').notEmpty();
  req.checkBody('password', 'Password is required').notEmpty();
  req.checkBody('name', 'Name is required').notEmpty();
  //req.assert('password2', 'Passwords do not match').equals(password);

  var errors = req.validationErrors(); 


  if(errors){ 
    console.log(errors);
    res.render('signup', {
        errors:errors,
        user: req.session.user
    });
  }else {
      var newUser = new User({
          email: username,
          password: password,
          name: name,
          phonenumber: phonenumber,
          address: address
        });

      var newShoppingcart = new Shoppingcart({
          id: username
      });

        User.createUser(newUser, function(err, user){
           if (err) throw err;
               console.log("new user has been created");
            });
        Shoppingcart.createShoppingCart(newShoppingcart, function(err, shoppingcart){
           if (err) throw err;
               console.log("new shoppingcart has been created");
            });
        res.redirect('/users/login');

        }   
  }
);

router.get('/profile', authentication);
router.get('/profile', function(req, res){
    //console.log(req.user);
    res.render('profile', {
      user: req.user
    });
});

router.get('/updateInfo', authentication);
router.get('/updateInfo', function(req, res){
  res.render('updateInfo', { user: req.user });
})

router.post('/updateInfo', authentication);
router.post('/updateInfo', function(req, res){
  var updateData ={
      name: req.body.name,
      phonenumber: req.body.phonenumber,
      address: req.body.address
  }

  User.update({email: req.session.user.email }, updateData, function(err,affected) {
    if(err) {throw err}
    else {
      req.session.user.name = req.body.name;
      req.session.user.phonenumber = req.body.phonenumber;
      req.session.user.address = req.body.address;
    }
    console.log('affected rows %d', affected);
  });
  
  res.redirect('/users/profile');
})



router.get('/reset', authentication);
router.get('/reset', function(req,res){
  res.render('resetpassword', {user: req.user});
})


router.get('/updatepassword/:uid', function(req,res,next){
  var uid = req.params.uid;
  console.log(uid);
  res.render('updatepassword', { userid: uid });
});

router.post('/updatepassword', function(req,res){
  var uid = req.body.uid;
  var password = req.body.password;
  var password1 = req.body.password1;
  console.log(uid);
  User.updatePassword(uid, password, function(err,affected){
    console.log('affected rows %d', affected);
  });
  res.redirect('/users/login');
});




//ResetPassword 
router.post('/reset', authentication);
router.post('/reset', function(req, res, next) {

    var transporter = nodemailer.createTransport(smtpTransport(options))

    var mailOptions = {
      to: req.session.user.email,
      from: 'ShopOnline',
      subject: 'ShopOnline Password Reset',
      text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
        'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
        'http://' + req.headers.host + '/users/updatepassword/' + req.session.user._id + '\n\n' +
        'If you did not request this, please ignore this email and your password will remain unchanged.\n'
    };
    transporter.sendMail(mailOptions, function(err) {
      if(err) {throw err}
      else {
      console.log('info', 'An e-mail has been sent to ' + req.session.user.email + ' with further instructions.');
      }
    });
    res.redirect('/users/profile');

})


router.get('/shoppingcart', authentication);
router.get('/shoppingcart', function(req,res){
  Shoppingcart.findOne({id: req.session.user.email },{},function(err, shoppingcart){
      console.log(shoppingcart);
      Discount.find({userid: req.session.user.email }, {}, function(err, discount){
          console.log(discount);
          res.render('shoppingcart', {
            user: req.session.user,
            shoppingcart: shoppingcart,
            discount: discount
          });
      });
  });
});

router.post('/addProductToShoppingCart', authentication);
router.post('/addProductToShoppingCart', function(req,res){
    var pid = req.body.pid;
    Product.findOne({ pid: pid}, function(err, product){
      if(err) {throw err}
      else{
        Shoppingcart.findOne({ id: req.session.user.email }, function(err, shoppingcart){
          if(err) {throw err}
          else{
              var tag = true;
              for(var i=0; i<shoppingcart.product.length;i++){
                if(pid == shoppingcart.product[i].pid) {
                    shoppingcart.product[i].quantity = shoppingcart.product[i].quantity + 1;
                    shoppingcart.save(function(err){
                      if (err) {throw err}
                    });
                    tag = false;
                }
              }
              if (tag){
                shoppingcart.product.push({
                  pid: product.pid,
                  name: product.name,
                  price: product.price,
                  picture: product.picture,
                  quantity: 1
                });
                shoppingcart.save(function(err){
                  if(err) {throw err}
                  else{
                    console.log("product has been successfully add into shoppingcart");
                  }
                });
              }              
            }
        })

        
        //req.session.notification = "product has been successfully add into shoppingcart";
        res.redirect('/productlist');
      } 
    });
});

router.get('/delFromCart/:pid', authentication);
router.get('/delFromCart/:pid', function(req, res) {
   Shoppingcart.findOne({id:req.session.user.email},function(error,shoppingcart){
      if(err) {throw err}
      else{
        console.log("hi");
        shoppingcart.product.remove('pid', req.params.pid);
        shoppingcart.save(function(err){
          if(err) {throw err}
        });
      }
   });
   res.redirect('/users/shoppingcart');
});

router.get('/deleteAccount', authentication);
router.get('/deleteAccount', function(req, res){
  res.render('deleteAccount', {
    user: req.session.user
  });
});

router.post('/deleteAccount', authentication);
router.post('/deleteAccount', function(req, res){
  User.remove({ email: req.session.user.email }, function (err) {
    if (err) {throw err;}
    else {
      Shoppingcart.remove({ id: req.session.user.email }, function(err){
        if (err) {throw err;}
        else {
          req.session.user = null;
          req.session.error = null;
          res.render('login', {
            user: req.session.user,
            error: "The account has been deleted"
           });
        }
      })
    }
  })
});

//ForgetPassword Email
// router.post('/forget', function(req, res, next) {
//   console.log("====");
//   async.waterfall([
//     function(done) {
//       crypto.randomBytes(20, function(err, buf) {
//         var token = buf.toString('hex');
//         done(err, token);
//       });
//     },
//     function(token, done) {
//       User.findOne({ email: req.body.email }, function(err, user) {
//         if (!user) {
//           req.flash('error', 'No account with that email address exists.');
//           return res.redirect('/users/profile');
//         }

//         user.resetPasswordToken = token;
//         user.resetPasswordExpires = Date.now() + 3600000; // 1 hour

//         user.save(function(err) {
//           done(err, token, user);
//         });
//       });
//     },
//     function(token, user, done) {
//       var smtpTransport = nodemailer.createTransport('SMTP', {
//         service: 'gmail',
//         auth: {
//           user: 'maodi101@gmail.com',
//           pass: '15150999623maodi'
//         }
//       });
//       var mailOptions = {
//         to: user.email,
//         from: 'ShopOnline',
//         subject: 'ShopOnline Password Reset',
//         text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.\n\n' +
//           'Please click on the following link, or paste this into your browser to complete the process:\n\n' +
//           'http://' + req.headers.host + '/reset/' + token + '\n\n' +
//           'If you did not request this, please ignore this email and your password will remain unchanged.\n'
//       };
//       smtpTransport.sendMail(mailOptions, function(err) {
//         req.flash('info', 'An e-mail has been sent to ' + user.email + ' with further instructions.');
//         done(err, 'done');
//       });
//     }
//   ], function(err) {
//     if (err) return next(err);
//     res.redirect('users/profile');
//   });
// });



function authentication (req, res, next) {

    if (!req.session.user) {
      req.session.error = null;
      return res.redirect('/users/login');
    }
    next();
}

function notAuthentication (req, res, next) {
    if (req.session.user) {
        req.session.error = 'Already logged in';
        return res.redirect('/users/profile');
    }
    next();
}


module.exports = router;
