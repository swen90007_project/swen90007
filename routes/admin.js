var express = require('express');

var User = require('../models/user');
var Discount = require('../models/discount');
var router = express.Router();

router.get('/', function(req,res){
	res.render('dashboard');
});

router.get('/listdiscount', function(req,res){
	Discount.find({}, {}, function(err, discount){
        if(err) {throw err}
        else{
        	console.log(discount);
  		    res.render('listdiscount', {
            discount: discount
          });
  	    }
    });  
})


router.get('/addNewDiscount', function(req, res){
	User.find({}, {}, function(err, user){
		res.render('adddiscount', {
			users: user,
			errors: null
		});
	});
    
});

router.post('/creatediscount', function(req, res){
	var discountname = req.body.name;
	var discountdescription = req.body.description;
	var discounttype = req.body.selectpicker;
	var offrate = req.body.offrate;
	var startdate = new Date(req.body.startdate);
	var enddate = new Date(req.body.enddate);
	var userid = [];
	for(var i=0; i<req.body.chkItem.length ;i++){
		userid.push(req.body.chkItem[i]);
	}
	var discountid = discountname.split(" ")[0] + discounttype.split(" ")[0]+req.body.startdate;
	//validation
	req.checkBody('name', 'Discount name is required').notEmpty();
	req.checkBody('description', 'Discount description is required').notEmpty();
	req.checkBody('startdate', 'Start date is required').notEmpty();
	req.checkBody('enddate', 'End date is required').notEmpty();

	var errors = req.validationErrors(); 

	if(errors){ 
		console.log(errors);
		User.find({}, {}, function(err, user){
			res.render('adddiscount', {
				users: user,
				errors: errors
			});
		});
	}
	else{
		var newdiscount = new Discount({
			id: discountid,
			name: discountname,
			description: discountdescription,
			discounttype: discounttype,
			offrate: offrate,
			userid: userid,
			startdate: startdate,
			enddate: enddate
		});

		newdiscount.save(function(err){
			if(err) {throw err}
		});
		res.redirect('/admin');
	}

});



module.exports = router;
